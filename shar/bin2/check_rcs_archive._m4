#!/usr/bin/env bash

# --------------------------------------------------------------------
# Synopsis: Check rcs archive by 'co -p' of all versions.
# --------------------------------------------------------------------
# Usage: $ourname RCS_ARCHIVE_PATHNAME
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2013 _Tom_Rodman_email
# --------------------------------------------------------------------

# -- Software License --
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2012/02/04 14:19:13 $   (GMT)
# $Revision: 1.29 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/main-2010.11.26/lib/sh/RCS/_29r_sh.m4.sed-src,v $
#      $Log: _29r_sh.m4.sed-src,v $
#
# --------------------------------------------------------------------

## written for cygwin, so forks minimized, and sleeps added

set -ue

ourname=${0##*/}
err_flag_file=$(mktemp /tmp/${ourname}.err_flag.XXXXXXXX)

TMPF=$(mktemp /tmp/$ourname.XXXXXX)
trap 'rm -f $TMPF $err_flag_file' EXIT

archive=$1

revs=$( 
  set -o pipefail
  
  rlog $archive |
  awk '/^revision / {print $2}' 
)

while read rev;
do 
  sleep .05
  ( 
  if tmp=$(exec 2>$TMPF; set -x; co -p$rev $archive);then
    sleep .05
    lines=$( wc -l <<<"$tmp" )
    sleep .05

    cat $TMPF
    echo "[${lines} lines]"
  else
    echo co: ERROR, retval: $?
    cat $TMPF
    echo . >> $err_flag_file
  fi

  echo 
  )
done <<<"$revs"

[[ -s $err_flag_file ]] && exit 1 || exit 0
