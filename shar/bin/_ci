#!/usr/bin/env bash
set -eu

# --------------------------------------------------------------------
# Synopsis: Wrapper for rcs ci/co. Updates ",v file", never touches 
# source file.
# --------------------------------------------------------------------
# Usage: $ourname -[ulv] FILE-PATHNAME
# --------------------------------------------------------------------
# Options
#   -l       passed to rcs 'ci'
#   -u       passed to rcs 'ci'
#   -v       assure user that only ,v file modified
#   -A       (iconv) convert file to ASCII before check in
# --------------------------------------------------------------------
# Rating: tone: tool used: often stable: y TBDs: n
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2009, 2010, 2012 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

# ***************************************************************************
# $Source: /usr/local/7Rq/package/cur/cm-2012.01.15/shar/bin/RCS/_ci._m4,v $
# $Date: 2013/03/09 12:28:59 $
# $Revision: 1.11 $    Last Chgd by:  $Author: rodmant $
# Author:  ....
# User:    ....
# Subject: .... 
# ***************************************************************************

# *****************************************************************
# define env var dir-defs for our "uqjau tools"
# *****************************************************************
cd ${0%/*}/../../
# $(dirname $0)
  # cd 2 dirs up from ( ${0%/*} == $(dirname $0) )
  # if  $0 has no "/"; 'source' below fails/we abort, that's OK (we do not support . in path)
_29r=$PWD
cd "$OLDPWD"

cd ${0%/*}
   # dirname of $0
_29rev=${PWD##*/}
   # Basename of dir below 'commands', 'scommands' or 'lib'; it contains the soft link
   # to this script.
cd "$OLDPWD"

if ! test -f $_29r/package/$_29rev/main/etc/dirdefs; then
  # create default dirdefs 
  source $_29r/package/$_29rev/main/slib/seed_dirdefs.shinc
else
  source $_29r/package/$_29rev/main/etc/dirdefs
    # not security risk if $0 is in correct commands dir
    # dirdefs appends $_29rev suffix to some defs
fi

#--

source $_29lib/bash_common.shinc
  # for summary of what is provided run this:
  #   sed -n '2,/-end-of-help/p' $_lib/bash_common.shinc|less

source $_29lib/basename.shinc

# **************************************************
# Main procedure.
# **************************************************
#main
{

_email_on_unexpected_exit=${_email_on_unexpected_exit:-yes}
_email_if_dying_on_trapped_sig=${_email_if_dying_on_trapped_sig:-no}


# ==================================================
# Process optional parameters.
# ==================================================
opt_true=1
OPT_u="" OPT_l="" OPT_v="" OPT_A=""
while getopts :Aulv OPTCHAR
 do 
    # Check for errors.
    case $OPTCHAR in
      \?) >&2 echo "Unexpected option(-$OPTARG)."
          exit 1
          # OPTCHAR is set to '?' if a nonexistent switch is specified
          # and 1st arg to getopts starts with ":"
       ;;
       :) >&2 echo "Missing parameter for option(-$OPTARG)."
          exit 1;
       ;;
    esac
     
    # Record the info in an "OPT_*" env var.
    eval OPT_${OPTCHAR}="\"\${OPTARG:-$opt_true}\""
 done
  
 shift $(($OPTIND - 1))  #notice we're outside the while
 unset opt_true OPTCHAR OPTIND
# If OPTIND not unset (or set to "1"), then subsequent getopts fail.

# #################################################################### 
# IMPORTANT
#   by design this script should *never* modify content or
#   perms of "$file"
#
#   To check this, search this script in vi for 
#     \${\?file[^,]                       # ignore this: }
# #################################################################### 

# **************************************************
# script start:
# **************************************************
pn=$1

if ! [[ -s $pn ]]; then
  echo "$ourname: [$pn] empty or does not exist." >&2
  exit 1
fi

filepathname="$(canonicalPath_ "$pn")"
cd "$(_dirname "$filepathname")"
echo "canonical parent dir: [$PWD]"

file="$(_basename "$pn")"

startdir=$PWD

# --------------------------------------------------------------------
# define $ci_sw from CLI
# --------------------------------------------------------------------
if [[ $OPT_l = 1 ]]
then
  ci_sw=-l
elif [[ $OPT_u = 1 ]]
then
  ci_sw=-u
else
  echo $ourname: -l or -u required >&2
  exit 1
fi

# assure user if verbose:
if [[ -n $OPT_v ]];then
  echo $ourname: \[$file] not touched, only the ,v file is modified. >&2
fi

# -------------------------------------------------------------------- 
# Find current RCS archive.
#   Favor pre-existing ./RCS dir archive over "same-dir" archive.
#   Create ./RCS dir, if no archive exists.
# -------------------------------------------------------------------- 
unset archive archiveexists
if [[ -f RCS/$file,v ]]
then
  archive="RCS/$file,v"
  archiveexists=1
elif [[ -f $file,v ]]
then
  archive="$file,v"
  archiveexists=1
else
  mkdir -p RCS
  archive="RCS/$file,v"
fi

# --------------------------------------------------------------------
# if archive exists make rcs lock
# --------------------------------------------------------------------
if [[ -n "${archiveexists:-}" ]]
then
  rcs -l -M "$archive"
    # No problem, if we already had a lock.
    # Does "rcs -u" of lock held by another (TBD: test this).
    # -M => do not send warning email to lock holder
fi

# --------------------------------------------------------------------
# ci in a temp dir
# --------------------------------------------------------------------

myTMPDIR=$(
  perl -e '
    use File::Temp "mkdtemp";
    $tmpdir = mkdtemp( "'/tmp/$ourname.XXXXXX'" );
    print "$tmpdir\n";
    '
)

tmparchive=$myTMPDIR/"$file,v"

rmTMPDIR()
{ rm -f "$tmparchive" "${myTMPDIR:-/OOPs_script_design_error}/$file"
  rmdir $myTMPDIR
}

_utf8 ()
{
    : personal function _func_ok2unset_;
    local tmpf=$(mktemp /tmp/$FUNCNAME.XXXXXXXXX);
    cat - > $tmpf;
    iconv -f utf-8 -t ${1:-ascii} -c $tmpf;
    : -c When this option is given, characters that cannot be converted are silently;
    : discarded, instead of leading to a conversion error.
}

_ci()
{
  # --------------------------------------------------------------------
  # Identify current archive if any.
  # cp file and archive if any, to $myTMPDIR
  # 
  #   (copies should *not* preserve ownership)
  # -------------------------------------------------------------------- 
    if [[ -n "${archiveexists:-}" ]]
    then
      cp "$archive" "$file" "${myTMPDIR}"
    else 
      cp "$file"            "${myTMPDIR}"
    fi

  # --------------------------------------------------------------------
  # convert to ASCII if $OPT_A defined
  # --------------------------------------------------------------------
    if [[ -n ${OPT_A:-} ]];then
      tmpf2ASCII=$(mktemp /tmp/$ourname.2ASCII.XXXXXX)
      (
      set -eu
      set -o pipefail
      cd "${myTMPDIR}"
      _utf8 < "$file" | (cat - > $tmpf2ASCII && cat $tmpf2ASCII > "$file")
      ) || { echo $ourname:ERROR: when converting to ASCII; exit 1; }
      rm -f $tmpf2ASCII
      echo "$ourname: [$myTMPDIR/$file] converted to ASCII"
    fi

  # --------------------------------------------------------------------
  # do checkin in $myTMPDIR
  #   (notice we do *not* create an RCS dir below $myTMPDIR)
  # --------------------------------------------------------------------
    cd "${myTMPDIR}"
    ci ${ci_sw} "$file"
  
  # ====================================================================
  #   update or create the final "$archive"
  # ====================================================================
    cd "$startdir" 
      # needed since "$archive" path is relative

    if ! [[ -f "$archive" ]]
    then
      (umask 0022;touch "$archive")
        # 0022 => u=rwx,g=rx,o=rx 
        # start w/owner writable archive
      chmod --reference "$file" "$archive"
        # adjust archive perms to match "$file"
    fi

    chmod u+w "$archive" 
      #owner writable, we need to update it
    cat "$tmparchive" > "$archive"

    chmod a-w "$archive"
      # rcs archives are read only

  # --------------------------------------------------------------------
  # cleanup
  # --------------------------------------------------------------------
    rmTMPDIR
}

## main action
_ci "$@"

# ==================================================
# main done.
# ==================================================
exit 0

} #main end
